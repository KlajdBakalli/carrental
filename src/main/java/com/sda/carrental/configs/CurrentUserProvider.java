package com.sda.carrental.configs;

import com.sda.carrental.model.CurrentUser;
import com.sda.carrental.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class CurrentUserProvider {

    @Autowired
    private UserRepository userRepository;

    public CurrentUser getCurrentUser() {
        if (SecurityContextHolder.getContext().getAuthentication() == null || SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {
            return new CurrentUser();
        } else {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = ((UserDetails) principal).getUsername();
            return userRepository.getCurrentUserByUsername(username);
        }
    }

}
