package com.sda.carrental.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "reservation")
public class ReservationEntity extends BaseEntity{


    private LocalDate dateOfBooking;
    private LocalDate dataFrom;
    private LocalDate dateTo;
    private Long amount;


    @ManyToOne
    @JoinColumn(name = "returnBranchId")
    private BranchEntity returnBranch;

    @ManyToOne
    @JoinColumn(name = "carId")
    private CarEntity carEntity;

}
