package com.sda.carrental.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "branch")
public class BranchEntity extends BaseEntity {

    private String address;
    private Long price;

    @ManyToOne
    @JoinColumn(name = "rentalId")
    private RentalEntity rentalEntity;

    @OneToMany(mappedBy = "branchEntity",fetch = FetchType.EAGER)
    private List<UserEntity> userEntities;

    @OneToMany(mappedBy = "branchEntity")
    private List<CarEntity> carEntities;

    @OneToMany(mappedBy = "returnBranch")
    private List<ReservationEntity> returnBranch;

}
