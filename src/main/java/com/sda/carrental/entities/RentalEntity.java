package com.sda.carrental.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "rental")
public class RentalEntity extends BaseEntity {


    private String name;

    private String internetDomain;

    private String contactAddress;

    private String owner;

    private String logoType;

    @OneToMany(mappedBy = "rentalEntity")
    private List<BranchEntity> branchEntities;


}
