package com.sda.carrental.entities;

import com.sda.carrental.entities.enume.Status;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "car")
public class CarEntity extends BaseEntity {


    private String brand;

    private String model;

    private String bodyType;

    private int year;

    private String colour;

    private Long mileage;

    private Long amount;

    @ManyToOne
    @JoinColumn(name = "branchId")
    private BranchEntity branchEntity;

    @OneToMany(mappedBy = "carEntity")
    private List<ReservationEntity> reservationEntities;

    @OneToMany(mappedBy = "carEntity")
    private List<CarStatusEntity> carStatusEntities;

}
