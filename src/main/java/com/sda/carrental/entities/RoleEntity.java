package com.sda.carrental.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "role")
public class RoleEntity extends BaseEntity {

    private String postion;
    private String description;

    @ManyToMany(mappedBy = "roleEntities")
    private List<UserEntity> userEntity;


}
