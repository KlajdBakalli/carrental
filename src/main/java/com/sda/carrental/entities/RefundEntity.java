package com.sda.carrental.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "refund")
public class RefundEntity extends BaseEntity {


    private Long surcharge;
    private LocalDate dateOfReturn;
    @ManyToOne
    @JoinColumn(name = "userId")
    private UserEntity userEntity;


}
