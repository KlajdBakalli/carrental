package com.sda.carrental.entities;

import com.sda.carrental.entities.enume.Status;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "carstatus")
public class CarStatusEntity extends BaseEntity {

    private Status status;
    private LocalDate date;

    @ManyToOne
    @JoinColumn(name = "carId")
    private CarEntity carEntity;


}
