package com.sda.carrental.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user")
public class UserEntity extends BaseEntity {

    private String username;
    private String password;
    private String email;
    private String address;
    private String firstName;
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "branchId")
    private BranchEntity branchEntity;


    @OneToMany(mappedBy = "userEntity")
    private List<RefundEntity> refundEntities;

    @ManyToMany
    @JoinTable(
            name = "userRole",
            joinColumns = @JoinColumn(name = "userID"),
            inverseJoinColumns = @JoinColumn(name = "roleId"))
    private List<RoleEntity> roleEntities;

}
