package com.sda.carrental.mappers;

import com.sda.carrental.entities.BranchEntity;
import com.sda.carrental.entities.RentalEntity;
import com.sda.carrental.entities.UserEntity;
import com.sda.carrental.model.dto.BranchDto;
import com.sda.carrental.model.dto.RentalDto;
import com.sda.carrental.model.request.BranchParamsRequest;
import com.sda.carrental.model.request.RentalParamsRequest;
import com.sda.carrental.model.request.UserParamsRequest;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RentalMapper {
    RentalDto convertToDto(RentalEntity rentalEntity);

    List<RentalDto> convertListToDto(List<RentalEntity> rentalEntityList);

   RentalEntity convertToEntity (RentalParamsRequest rentalParamsRequest);

    void update(@MappingTarget RentalEntity rentalEntity, RentalParamsRequest rentalParamsRequest);
}
