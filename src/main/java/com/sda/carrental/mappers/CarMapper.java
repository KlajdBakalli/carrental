package com.sda.carrental.mappers;

import com.sda.carrental.entities.CarEntity;
import com.sda.carrental.model.dto.CarFilterDto;
import com.sda.carrental.model.dto.CarsDto;
import com.sda.carrental.model.dto.UserDto;
import com.sda.carrental.model.request.CarParamsRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CarMapper {
@Mapping(source ="branchEntity.id",target = "branchId")
    CarsDto convertToDto(CarEntity carEntity);

    List<CarsDto> convertListToDto(List<CarEntity> carEntityList);

    List<CarFilterDto> convertListToFilterDto(List<CarEntity> carEntityList);

    @Mapping(source = "branchId", target = "branchEntity.id")
    CarEntity convertToEntity(CarParamsRequest carParamsRequest);


    void update(@MappingTarget CarEntity carEntity, CarParamsRequest carParamsRequest);
}
