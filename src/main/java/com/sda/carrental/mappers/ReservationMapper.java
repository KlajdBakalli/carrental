package com.sda.carrental.mappers;

import com.sda.carrental.entities.ReservationEntity;
import com.sda.carrental.entities.UserEntity;
import com.sda.carrental.model.dto.ReservationDto;
import com.sda.carrental.model.request.ReservationParamsRequest;
import com.sda.carrental.model.request.UserParamsRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ReservationMapper {

    @Mapping(source = "carEntity.id",target = "carId")
    @Mapping(source = "returnBranch.id",target = "branchReturnId")
    ReservationDto convertToDto(ReservationEntity reservationEntity);

    List<ReservationDto> convertListToDto(List<ReservationEntity> reservationEntityList);


    ReservationEntity convertToEntity(ReservationParamsRequest reservationParamsRequest);

    void update(@MappingTarget ReservationEntity reservationEntity, ReservationParamsRequest reservationParamsRequest);
}
