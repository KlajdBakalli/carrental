package com.sda.carrental.mappers;


import com.sda.carrental.entities.BranchEntity;
import com.sda.carrental.model.dto.BranchDto;
import com.sda.carrental.model.request.BranchParamsRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")

public interface BranchMapper {

    BranchDto convertToDto(BranchEntity branchEntity);

    List<BranchDto> convertListToDto(List<BranchEntity> branchEntityList);

    @Mapping(source = "rentalId", target = "rentalEntity.id")
    BranchEntity convertToEntity(BranchParamsRequest branchParamsRequest);

    void update(@MappingTarget BranchEntity branchEntity, BranchParamsRequest branchParamsRequest);

}
