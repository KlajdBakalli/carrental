package com.sda.carrental.mappers;

import com.sda.carrental.entities.UserEntity;
import com.sda.carrental.model.dto.UserDto;
import com.sda.carrental.model.request.UserParamsRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsersMapper {

    UserDto convertToDto(UserEntity userEntity);

    List<UserDto> convertListToDto(List<UserEntity> userEntityList);

    @Mapping(source = "branchId", target = "branchEntity.id")
    UserEntity convertToEntity(UserParamsRequest userParamsRequest);

    void update(@MappingTarget UserEntity userEntity, UserParamsRequest userParamsRequest);
}
