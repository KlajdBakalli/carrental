package com.sda.carrental.model.dto;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CarsDto {

    private Long id;

    private String brand;

    private String model;

    private String bodyType;

    private int year;

    private String colour;

    private Long mileage;

    private Long amount;

    private Long branchId;
}
