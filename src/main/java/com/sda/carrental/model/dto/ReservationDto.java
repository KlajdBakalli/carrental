package com.sda.carrental.model.dto;

import lombok.*;

import java.time.LocalDate;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Builder
public class ReservationDto {
    private Long id;
    private LocalDate dateOfBooking;
    private LocalDate dataFrom;
    private LocalDate dateTo;
    private Long amount;
    private Long carId;
    private Long branchReturnId;

}
