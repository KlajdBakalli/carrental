package com.sda.carrental.model.dto;


import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CarFilterDto {

    private String model;

    private int year;

    private String colour;

    private String brand;
}
