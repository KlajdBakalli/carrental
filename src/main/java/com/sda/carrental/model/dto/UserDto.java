package com.sda.carrental.model.dto;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private Long id;
    private  String email;
    private String address;
    private String firstName;
    private String lastName;
}
