package com.sda.carrental.model.dto;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RentalDto {
    private Long id;

    private String name;

    private String internetDomain;

    private String contactAddress;

    private String owner;

    private String logoType;
}
