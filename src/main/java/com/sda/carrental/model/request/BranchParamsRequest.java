package com.sda.carrental.model.request;

import com.sda.carrental.model.dto.RentalDto;
import lombok.*;

import javax.validation.constraints.NotNull;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BranchParamsRequest {


    private String address;
    private Long rentalId;

}
