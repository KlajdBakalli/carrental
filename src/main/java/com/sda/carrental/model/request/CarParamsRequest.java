package com.sda.carrental.model.request;

import com.sda.carrental.model.dto.BranchDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarParamsRequest {
    private String brand;

    private String model;

    private String bodyType;

    private int year;

    private String colour;

    private Long mileage;

    private Long amount;

    private Long branchId;
}
