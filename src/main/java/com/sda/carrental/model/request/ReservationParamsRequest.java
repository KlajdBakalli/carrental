package com.sda.carrental.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReservationParamsRequest {

    private LocalDate dateOfBooking;
    private LocalDate dataFrom;
    private LocalDate dateTo;
    private Long amount;
    private Long returnBranchId;
    private Long carId;

}
