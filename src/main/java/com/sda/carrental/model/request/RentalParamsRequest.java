package com.sda.carrental.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RentalParamsRequest {

    private String name;
    private String internetDomain;
    private String contactAddress;
    private String owner;
    private String logoType;
}
