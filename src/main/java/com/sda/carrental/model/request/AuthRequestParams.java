package com.sda.carrental.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Builder
@Data
public class AuthRequestParams {

    private String username;
    private String password;
}
