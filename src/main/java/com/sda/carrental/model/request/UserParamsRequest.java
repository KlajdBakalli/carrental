package com.sda.carrental.model.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class UserParamsRequest {

    private String username;
    private String password;
    private String email;
    private String address;
    private String firstName;
    private String lastName;
    private Long branchId;
}
