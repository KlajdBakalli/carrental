package com.sda.carrental.repository;

import com.sda.carrental.entities.CarEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<CarEntity, Long> {

    List<CarEntity> findByModelAndYearAndColour(final String model, final int year,final String colour);
}