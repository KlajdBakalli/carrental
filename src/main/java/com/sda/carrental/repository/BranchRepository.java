package com.sda.carrental.repository;

import com.sda.carrental.entities.BaseEntity;
import com.sda.carrental.entities.BranchEntity;
import com.sda.carrental.entities.RentalEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BranchRepository extends JpaRepository<BranchEntity,Long> {


}
