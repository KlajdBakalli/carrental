package com.sda.carrental.controller;

import com.sda.carrental.model.dto.RentalDto;
import com.sda.carrental.model.dto.UserDto;
import com.sda.carrental.model.request.RentalParamsRequest;
import com.sda.carrental.model.request.UserParamsRequest;
import com.sda.carrental.service.impl.RentalServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
public class RentalController {

    @Autowired
    private RentalServiceImpl rentalService;

    @PostMapping("/rental")
    public ResponseEntity<RentalDto> createNewRental(@RequestBody RentalParamsRequest rentalParamsRequest) {
        RentalDto rentalDto = rentalService.save(rentalParamsRequest);
        return ResponseEntity.ok(rentalDto);
    }

    @PutMapping("/rental/{rentalId}")
    public ResponseEntity<RentalDto> rentalUser(@PathVariable Long rentalId, @RequestBody RentalParamsRequest rentalParamsRequest) {
        RentalDto rentalDto = (RentalDto)  rentalService.update(rentalId, rentalParamsRequest);
        return ResponseEntity.ok(rentalDto);
    }

    @DeleteMapping("/rental/{rentalId}")
    public void deleteRental(@PathVariable("rentalId") Long rentalId) {rentalService.delete(rentalId);
    }

    @GetMapping("/rental")
    public ResponseEntity<List<RentalDto>> getAll() {
        List<RentalDto> rentalDtoList = rentalService.readAll();
        return ResponseEntity.ok(rentalDtoList);
    }
}
