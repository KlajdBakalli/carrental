package com.sda.carrental.controller;

import com.sda.carrental.model.dto.UserDto;
import com.sda.carrental.model.request.UserParamsRequest;
import com.sda.carrental.service.UserService;
import com.sda.carrental.service.impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PutMapping("/user/{userId}")
    public ResponseEntity<UserDto> updateUser(@PathVariable Long userId, @RequestBody UserParamsRequest userParamsRequest) {
        UserDto userDto = (UserDto) userService.update(userId, userParamsRequest);
        return ResponseEntity.ok(userDto);
    }

    @PostMapping("/user")
    public ResponseEntity<UserDto> createNewUser(@RequestBody UserParamsRequest userParamsRequest) {
        userParamsRequest.setPassword(passwordEncoder.encode(userParamsRequest.getPassword()));
        UserDto userDto = userService.save(userParamsRequest);
        return ResponseEntity.ok(userDto);
    }

    @DeleteMapping("/user/{userId}")
    public void deleteUser(@PathVariable("userId") Long userId) {
        userService.delete(userId);
    }

    @GetMapping("/user")
    public ResponseEntity<List<UserDto>> getAll() {
        List<UserDto> userDtoList = userService.readAll();
        return ResponseEntity.ok(userDtoList);
    }
}
