package com.sda.carrental.controller;

import com.sda.carrental.model.dto.BranchDto;
import com.sda.carrental.model.dto.CarsDto;
import com.sda.carrental.model.dto.ReservationDto;
import com.sda.carrental.model.request.BranchParamsRequest;
import com.sda.carrental.model.request.CarParamsRequest;
import com.sda.carrental.service.impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
public class BranchController {
    @Autowired
    private BranchServiceImpl branchService;



    @PostMapping("/branch")
    public ResponseEntity<BranchDto> createNewBranch(@RequestBody BranchParamsRequest branchParamsRequest) {
        BranchDto branchDto = branchService.save(branchParamsRequest);
        return ResponseEntity.ok(branchDto);
    }

    @DeleteMapping("/branch/{branchId}")
    public void deleteBranch(@PathVariable("branchId") Long branchId) {
        branchService.delete(branchId);
    }

    @GetMapping("/branch")
    public ResponseEntity<List<BranchDto>> getAll() {
        List<BranchDto> branchDtoList = branchService.readAll();
        return ResponseEntity.ok(branchDtoList);
    }

    @PutMapping("/branch/{branchId}")
    public ResponseEntity<BranchDto> updateBranch(@PathVariable Long branchId, @RequestBody BranchParamsRequest branchParamsRequest) {
        BranchDto branchDto = branchService.update(branchId, branchParamsRequest);
        return ResponseEntity.ok(branchDto);
    }
}
