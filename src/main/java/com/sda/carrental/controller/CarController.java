package com.sda.carrental.controller;


import com.sda.carrental.model.dto.CarsDto;
import com.sda.carrental.model.request.CarParamsRequest;
import com.sda.carrental.service.impl.CarServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@CrossOrigin(origins = "*")
public class CarController {
    @Autowired
    private CarServiceImpl carService;



    @PostMapping("/car")
    public ResponseEntity<CarsDto> createNewCar(@RequestBody CarParamsRequest carParamsRequest) {
        CarsDto carDto = carService.save(carParamsRequest);
        return ResponseEntity.ok(carDto);
    }

    @DeleteMapping("/car/{carId}")
    public void deleteCar(@PathVariable("carId") Long carId) {
        carService.delete(carId);
    }

    @GetMapping("/car")
    public ResponseEntity<List<CarsDto>> getAll() {
        List<CarsDto> carDtoList = carService.readAll();
        return ResponseEntity.ok(carDtoList);
    }

    @PutMapping("/car/{carId}")
    public ResponseEntity<CarsDto> updateCar(@PathVariable Long carId, @RequestBody CarParamsRequest carParamsRequest) {
        CarsDto carsDto = carService.update(carId, carParamsRequest);
        return ResponseEntity.ok(carsDto);
    }
    @GetMapping("/car/{carId}")
    public  ResponseEntity<CarsDto> readByid(@PathVariable("carId") Long carId) {
        CarsDto carsDto=carService.readById(carId);
        return ResponseEntity.ok(carsDto);
    }
}


