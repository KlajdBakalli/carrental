package com.sda.carrental.controller;

import com.sda.carrental.model.dto.ReservationDto;
import com.sda.carrental.model.request.ReservationParamsRequest;
import com.sda.carrental.service.impl.ReservationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
public class ReservationController {
    @Autowired
    private ReservationServiceImpl reservationService;

    @GetMapping("/reserrvation")
    public ResponseEntity<List<ReservationDto>> getAll() {
        List<ReservationDto> reservationDtoList = reservationService.readAll();
        return ResponseEntity.ok(reservationDtoList);
    }
    @PostMapping("/reservation")
    public ResponseEntity<ReservationDto> creatNewReservation(@RequestBody ReservationParamsRequest reservationParamsRequest) {
        ReservationDto reservationDto=reservationService.save(reservationParamsRequest);
        return ResponseEntity.ok(reservationDto);
    }

    @DeleteMapping("/reservation/{reservationId}")
    public void deleteReservation(@PathVariable("reservationId") Long reservationId) {
        reservationService.delete(reservationId);
    }
    @PutMapping("/reservation/{reservationId}")
    public ResponseEntity<ReservationDto> updateReservation(@PathVariable Long reservationId, @RequestBody ReservationParamsRequest reservationParamsRequest) {
        ReservationDto reservationDto = (ReservationDto) reservationService.update(reservationId, reservationParamsRequest);
        return ResponseEntity.ok(reservationDto);
    }
}
