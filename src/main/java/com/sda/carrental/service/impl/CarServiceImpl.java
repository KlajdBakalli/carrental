package com.sda.carrental.service.impl;

import com.sda.carrental.entities.CarEntity;
import com.sda.carrental.exceptions.NotFoundException;
import com.sda.carrental.mappers.CarMapper;
import com.sda.carrental.model.dto.CarFilterDto;
import com.sda.carrental.model.dto.CarsDto;
import com.sda.carrental.model.request.CarParamsRequest;
import com.sda.carrental.repository.BranchRepository;
import com.sda.carrental.repository.CarRepository;
import com.sda.carrental.service.CarService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CarServiceImpl implements CarService<CarParamsRequest, CarsDto> {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private CarMapper carMapper;

    @Override
    public CarsDto save(CarParamsRequest carParamsRequest) {
        var carEntity= carMapper.convertToEntity(carParamsRequest);
        var branchEntity= branchRepository.findById(carParamsRequest.getBranchId())
                .orElseThrow(() -> new NotFoundException("not found"));
        carEntity.setBranchEntity(branchEntity);
        carRepository.save(carEntity);
        return carMapper.convertToDto(carEntity);
    }

    @Override
    public CarsDto update(Long carId, CarParamsRequest carParamsRequest) {

        var carEntity = carRepository.findById(carId)
                .orElseThrow(() -> new NotFoundException("Not found"));
        var branchEntity = branchRepository.findById(carParamsRequest.getBranchId())
                .orElseThrow(() -> new NotFoundException("Not found"));
        carEntity.setBranchEntity(branchEntity);
        carMapper.update(carEntity, carParamsRequest);
        carRepository.save(carEntity);
        return carMapper.convertToDto(carEntity);

    }

    @Override
    public void delete(Long carId) {
        carRepository.findById(carId)
                .orElseThrow(() -> new NotFoundException("User id not valid"));
        carRepository.deleteById(carId);
    }

    @Override
    public List<CarsDto> readAll() {
        var carEntity = carRepository.findAll();
        return carMapper.convertListToDto(carEntity);
    }

    @Override
    public List<CarFilterDto> filter(String model, int year, String colour) {
        List<CarEntity> carEntityList = carRepository.findByModelAndYearAndColour(model, year, colour);
        return carMapper.convertListToFilterDto(carEntityList);
    }

    @Override
    public CarsDto readById(Long carId) {
        var car = carRepository.findById(carId)
                .orElseThrow(() -> new NotFoundException("Not found"));
        return carMapper.convertToDto(car);
    }



}


