package com.sda.carrental.service.impl;

import com.sda.carrental.entities.UserEntity;
import com.sda.carrental.exceptions.NotFoundException;
import com.sda.carrental.mappers.UsersMapper;
import com.sda.carrental.model.dto.UserDto;
import com.sda.carrental.model.request.UserParamsRequest;
import com.sda.carrental.repository.BranchRepository;
import com.sda.carrental.repository.UserRepository;
import com.sda.carrental.service.UserService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService<UserParamsRequest, UserDto>, UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UsersMapper usersMapper;

    @Autowired
    private BranchRepository branchRepository;

    @Override
    public UserDto save(UserParamsRequest userParamsRequest) {
        var userEntity = usersMapper.convertToEntity(userParamsRequest);
        var branchEntity = branchRepository.findById(userParamsRequest.getBranchId())
                .orElseThrow(() -> new NotFoundException("NotFound"));
        userEntity.setBranchEntity(branchEntity);
        userRepository.save(userEntity);
        return usersMapper.convertToDto(userEntity);
    }

    @Override
    public UserDto update(Long userId, UserParamsRequest userParamsRequest) {
        var userEntity = userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("NotFound"));
        var branchEntity = branchRepository.findById(userParamsRequest.getBranchId())
                .orElseThrow(() -> new NotFoundException("NotFound"));
        userEntity.setBranchEntity(branchEntity);
        usersMapper.update(userEntity, userParamsRequest);
        userRepository.save(userEntity);
        return usersMapper.convertToDto(userEntity);
    }


    @Override
    public void delete(Long userId) {
        userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("User id not valid"));
        userRepository.deleteById(userId);
    }

    @Override
    public List<UserDto> readAll() {
        var userEntity = userRepository.findAll();
        return usersMapper.convertListToDto(userEntity);
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        Optional<UserEntity> userEntityOptional = userRepository.findByUsername(username);
        if (!userEntityOptional.isPresent()) {
            throw new UsernameNotFoundException(username);
        }
        return new User(
                userEntityOptional.get().getUsername(), userEntityOptional.get().getPassword(), new ArrayList<>());
    }
}
