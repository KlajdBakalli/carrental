package com.sda.carrental.service.impl;

import com.sda.carrental.exceptions.NotFoundException;
import com.sda.carrental.mappers.BranchMapper;
import com.sda.carrental.model.dto.BranchDto;
import com.sda.carrental.model.request.BranchParamsRequest;
import com.sda.carrental.repository.BranchRepository;
import com.sda.carrental.repository.RentalRepository;
import com.sda.carrental.service.BranchService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class BranchServiceImpl implements BranchService<BranchParamsRequest, BranchDto> {
    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private BranchMapper branchMapper;

    @Autowired
    private RentalRepository rentalRepository;

    @Override
    public BranchDto save(final BranchParamsRequest branchParamsRequest) {
        var branchEntity = branchMapper.convertToEntity(branchParamsRequest);
            var rentalEntity=rentalRepository.findById(branchParamsRequest.getRentalId())
                            .orElseThrow(() -> new NotFoundException("Sad"));
            branchEntity.setRentalEntity(rentalEntity);
        branchRepository.save(branchEntity);
        return branchMapper.convertToDto(branchEntity);
    }

    @Override
    public BranchDto update(Long branchId, BranchParamsRequest branchParamsRequest) {
        var branchEntity = branchRepository.findById(branchId)
                .orElseThrow(() -> new NotFoundException("Sad"));
        var rentalEntity = rentalRepository.findById(branchParamsRequest.getRentalId())
                .orElseThrow(() -> new NotFoundException("Sad"));
        branchEntity.setRentalEntity(rentalEntity);
        branchMapper.update(branchEntity, branchParamsRequest);
        branchRepository.save(branchEntity);
        return branchMapper.convertToDto(branchEntity);
    }

    @Override
    public BranchDto updateAmount(Long price, Long branchId) {
        var branchEntity = branchRepository.findById(branchId)
                .orElseThrow(() -> new NotFoundException("Sad"));
        branchEntity.setPrice(price);
        branchRepository.save(branchEntity);
        return branchMapper.convertToDto(branchEntity);
    }


    @Override
    public void delete(Long branchId) {
        branchRepository.findById(branchId)
                .orElseThrow(() -> new NotFoundException("User id not valid"));
        branchRepository.deleteById(branchId);
    }


    @Override
    public List<BranchDto> readAll() {
<<<<<<< HEAD


        return null;
=======
        var branchEntity = branchRepository.findAll();
        return branchMapper.convertListToDto(branchEntity);
>>>>>>> 4f3860cb28490acebbe3e11edf9ef8fada41868e
    }
}
