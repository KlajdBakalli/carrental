package com.sda.carrental.service.impl;

import com.sda.carrental.exceptions.NotFoundException;
import com.sda.carrental.mappers.RentalMapper;
import com.sda.carrental.model.dto.RentalDto;
import com.sda.carrental.model.request.RentalParamsRequest;
import com.sda.carrental.repository.RentalRepository;
import com.sda.carrental.service.BaseService;
import com.sda.carrental.service.RentalService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class RentalServiceImpl implements RentalService<RentalParamsRequest, RentalDto> {
    @Autowired
    private RentalRepository rentalRepository;

    @Autowired
    private RentalMapper rentalMapper;


    @Override
    public RentalDto save(final RentalParamsRequest rentalParamsRequest) {
        var rentalEntity = rentalMapper.convertToEntity(rentalParamsRequest);
        rentalRepository.save(rentalEntity);
        return rentalMapper.convertToDto(rentalEntity);

    }

    @Override
    public RentalDto update(Long rentalId, RentalParamsRequest rentalParamsRequest) {
        var rentalEntity = rentalRepository.findById(rentalId)
                .orElseThrow(() -> new NotFoundException("sad"));
        rentalMapper.update(rentalEntity,rentalParamsRequest);
        rentalRepository.save(rentalEntity);
        return rentalMapper.convertToDto(rentalEntity);
    }

    @Override
    public void delete(Long rentalId) {
        rentalRepository.findById(rentalId)
                .orElseThrow(() -> new NotFoundException("User id not valid"));
        rentalRepository.deleteById(rentalId);
    }


    @Override
    public List readAll() {
        var rentalEntity = rentalRepository.findAll();
        return rentalMapper.convertListToDto(rentalEntity);
    }
}
