package com.sda.carrental.service.impl;

import com.sda.carrental.exceptions.NotFoundException;
import com.sda.carrental.mappers.ReservationMapper;
import com.sda.carrental.model.dto.ReservationDto;
import com.sda.carrental.model.request.ReservationParamsRequest;
import com.sda.carrental.repository.BranchRepository;
import com.sda.carrental.repository.CarRepository;
import com.sda.carrental.repository.ReservationRepository;
import com.sda.carrental.service.BaseService;
import com.sda.carrental.service.BranchService;
import com.sda.carrental.service.ReservationService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Period;
import java.util.List;

@Service
@Transactional
public class ReservationServiceImpl implements ReservationService<ReservationParamsRequest, ReservationDto> {
    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private ReservationMapper reservationMapper;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private BranchService branchService;

    @Override
    public ReservationDto save(final ReservationParamsRequest reservationParamsRequest) {
        var branch = branchRepository.findById(reservationParamsRequest.getReturnBranchId()).orElseThrow(() -> new NotFoundException("branch not found"));
        var car = carRepository.findById(reservationParamsRequest.getCarId()).orElseThrow(() -> new NotFoundException("car not found"));
        var reservationEntity = reservationMapper.convertToEntity(reservationParamsRequest);
        reservationEntity.setCarEntity(car);
        reservationEntity.setReturnBranch(branch);
        reservationRepository.save(reservationEntity);
        var period = Period.between(reservationEntity.getDataFrom(), reservationEntity.getDateTo());
        var price = period.getDays() * reservationEntity.getCarEntity().getAmount();
        branchService.updateAmount(price, reservationEntity.getReturnBranch().getId());
        return reservationMapper.convertToDto(reservationEntity);
    }

    @Override
    public ReservationDto update(Long reservationId, ReservationParamsRequest reservationParamsRequest) {
        var reservationEntity = reservationRepository.findById(reservationId)
                .orElseThrow(() -> new NotFoundException("sad"));
        var branchEntity = branchRepository.findById(reservationParamsRequest.getReturnBranchId())
                .orElseThrow(() -> new NotFoundException("sad"));
       var carEntity= carRepository.findById(reservationParamsRequest.getCarId())
                       .orElseThrow(() -> new NotFoundException("sa"));
        reservationEntity.setReturnBranch(branchEntity);
        reservationEntity.setCarEntity(carEntity);
        reservationMapper.update(reservationEntity, reservationParamsRequest);
        reservationRepository.save(reservationEntity);
        return reservationMapper.convertToDto(reservationEntity);
    }

    @Override
    public void delete(Long reservationId) {
        reservationRepository.findById(reservationId)
                .orElseThrow(() -> new NotFoundException("User id not valid"));
        reservationRepository.deleteById(reservationId);
    }

    @Override
    public List<ReservationDto> readAll() {
        var reservationEntity = reservationRepository.findAll();
        return reservationMapper.convertListToDto(reservationEntity);
    }
}
