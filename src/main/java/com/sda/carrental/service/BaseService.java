package com.sda.carrental.service;

import java.util.List;

public interface BaseService<T, R> {

    R save(T t);

    R update(Long id, T t);

    void delete(Long id);

    List<R> readAll();
}