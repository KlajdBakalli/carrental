package com.sda.carrental.service;

import com.sda.carrental.model.dto.BranchDto;
import com.sda.carrental.model.request.BranchParamsRequest;
import com.sda.carrental.service.BaseService;

public interface BranchService<T,R> extends BaseService<T, R> {
     BranchDto updateAmount(Long price, Long branchId);
}
