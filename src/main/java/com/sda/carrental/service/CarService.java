package com.sda.carrental.service;

import com.sda.carrental.model.dto.CarFilterDto;
import com.sda.carrental.model.dto.CarsDto;

import java.util.List;

public interface CarService <T,R> extends BaseService<T, R>{

    List<CarFilterDto> filter(final String model, final int year,final String colour);
    CarsDto readById(Long carId);
}
